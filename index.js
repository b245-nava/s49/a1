// console.log('Happy Monday?');

// [Section] The Fetch keyword
	/*
		Syntax:
			fetch('url', {options});
			- {options} contains the method, body, and headers
	*/
	// GET post data
		
	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => {
		// console.log(response.json());
		return response.json();
		}) // Remember: using response.json() will result in another pending promise
	.then(result => {
		console.log(result);
		showPosts(result); 	// This time, we'll invoke the showPosts function here, since we already have the mock API captured in this fetch function.
	});

// [Section] Show Posts
	// Now, let's create a function to show the posts from our API.


	const showPosts = (posts) => {
		let entries = ``;

		posts.forEach((post) => {
			entries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
			`; // placing ${post.id} as the argument in editPosts() will pass the post's id as the function's argument.
		});

		document.querySelector('#div-post-entries').innerHTML = entries;
		// Here is where the posts from the API will be contained.
	};

// [Section] POST data on our API
	
	document.querySelector('#form-add-post').addEventListener('submit', (event) => {

		event.preventDefault();

		// Using the post request, fetch will return the newly-created document.
		fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			body: JSON.stringify({
				title: document.querySelector('#txt-title').value,
				body: document.querySelector('#txt-body').value,
				userId: 1
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);

			document.querySelector('#txt-title').value = null;
			document.querySelector('#txt-body').value = null;

			alert('Your post was successfully added!');
		})
	});

// [Section] Edit Post

	const editPost = (id) => {
		console.log(id);

		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		console.log(title);
		console.log(body);

		document.querySelector(`#txt-edit-title`).value = title;
		document.querySelector(`#txt-edit-body`).value = body;
		document.querySelector(`#txt-edit-id`).value = id;

		// The .removeAttribute() will remove the declared attribute from the element.
		document.querySelector(`#btn-submit-update`).removeAttribute('disabled');
	};

	document.querySelector(`#form-edit-post`).addEventListener('submit', (event) => {

		event.preventDefault();

		let id = document.querySelector(`#txt-edit-id`).value;

		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: document.querySelector(`#txt-edit-title`).value,
				id: id,
				body: document.querySelector(`#txt-edit-body`).value,
				userId: 1
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);

			alert('Your post has been successfully updated!');

			document.querySelector(`#txt-edit-title`).value = null;
			document.querySelector(`#txt-edit-body`).value = null;
			document.querySelector(`#txt-edit-id`).value = null;

			// .setAttribute does the oppostie of .removeAttribute
			document.querySelector(`#btn-submit-update`).setAttribute('disabled', true);
		});
	});

// [Section] Delete Posts
	const deletePost = (id) => {
		
		let element = document.getElementById(`post-${id}`);
		element.remove();

		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: 'DELETE'
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);
		});
	};

